--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

-- Started on 2019-02-14 13:55:38

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12355)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2150 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 186 (class 1259 OID 1253670)
-- Name: komentar; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE komentar (
    komentarid integer NOT NULL,
    isi text,
    create_at timestamp without time zone DEFAULT (now())::timestamp without time zone,
    userid integer,
    postid integer
);


ALTER TABLE komentar OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 1253668)
-- Name: komentar_komentarid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE komentar_komentarid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE komentar_komentarid_seq OWNER TO postgres;

--
-- TOC entry 2151 (class 0 OID 0)
-- Dependencies: 185
-- Name: komentar_komentarid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE komentar_komentarid_seq OWNED BY komentar.komentarid;


--
-- TOC entry 182 (class 1259 OID 1253597)
-- Name: pengguna; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE pengguna (
    userid integer NOT NULL,
    nama character varying(100),
    email character varying(100),
    username character varying(20),
    password character varying(50),
    jk character varying(1) DEFAULT 'L'::character varying,
    alamat character varying(250),
    tmplahir character varying(50),
    tgllahir date
);


ALTER TABLE pengguna OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 1253595)
-- Name: pengguna_userid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE pengguna_userid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pengguna_userid_seq OWNER TO postgres;

--
-- TOC entry 2152 (class 0 OID 0)
-- Dependencies: 181
-- Name: pengguna_userid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE pengguna_userid_seq OWNED BY pengguna.userid;


--
-- TOC entry 184 (class 1259 OID 1253623)
-- Name: postingan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE postingan (
    postid integer NOT NULL,
    isi text,
    create_at timestamp without time zone DEFAULT (now())::timestamp without time zone,
    userid integer
);


ALTER TABLE postingan OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 1253621)
-- Name: postingan_postid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE postingan_postid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE postingan_postid_seq OWNER TO postgres;

--
-- TOC entry 2153 (class 0 OID 0)
-- Dependencies: 183
-- Name: postingan_postid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE postingan_postid_seq OWNED BY postingan.postid;


--
-- TOC entry 187 (class 1259 OID 1253690)
-- Name: suka; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE suka (
    userid integer NOT NULL,
    postid integer NOT NULL,
    create_at timestamp without time zone DEFAULT (now())::timestamp without time zone
);


ALTER TABLE suka OWNER TO postgres;

--
-- TOC entry 2004 (class 2604 OID 1253673)
-- Name: komentarid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY komentar ALTER COLUMN komentarid SET DEFAULT nextval('komentar_komentarid_seq'::regclass);


--
-- TOC entry 2000 (class 2604 OID 1253600)
-- Name: userid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pengguna ALTER COLUMN userid SET DEFAULT nextval('pengguna_userid_seq'::regclass);


--
-- TOC entry 2002 (class 2604 OID 1253626)
-- Name: postid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY postingan ALTER COLUMN postid SET DEFAULT nextval('postingan_postid_seq'::regclass);


--
-- TOC entry 2141 (class 0 OID 1253670)
-- Dependencies: 186
-- Data for Name: komentar; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY komentar (komentarid, isi, create_at, userid, postid) FROM stdin;
1	kirim	2019-02-13 22:52:04.453905	3	2
\.


--
-- TOC entry 2154 (class 0 OID 0)
-- Dependencies: 185
-- Name: komentar_komentarid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('komentar_komentarid_seq', 1, true);


--
-- TOC entry 2137 (class 0 OID 1253597)
-- Dependencies: 182
-- Data for Name: pengguna; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY pengguna (userid, nama, email, username, password, jk, alamat, tmplahir, tgllahir) FROM stdin;
3	ismu	ismu@gmail.ismu	ismu	ismu	L	\N	\N	\N
1	Ahmad Dwi Alfian	ahmaddwialfian@gmail.com	admin	admin	L	jalan kusnandar	Bojonegoro	2019-02-13
\.


--
-- TOC entry 2155 (class 0 OID 0)
-- Dependencies: 181
-- Name: pengguna_userid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('pengguna_userid_seq', 3, true);


--
-- TOC entry 2139 (class 0 OID 1253623)
-- Dependencies: 184
-- Data for Name: postingan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY postingan (postid, isi, create_at, userid) FROM stdin;
2	halo bro, edited	2019-02-14 00:17:58	1
\.


--
-- TOC entry 2156 (class 0 OID 0)
-- Dependencies: 183
-- Name: postingan_postid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('postingan_postid_seq', 2, true);


--
-- TOC entry 2142 (class 0 OID 1253690)
-- Dependencies: 187
-- Data for Name: suka; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY suka (userid, postid, create_at) FROM stdin;
3	2	2019-02-13 22:06:13.543271
\.


--
-- TOC entry 2014 (class 2606 OID 1253679)
-- Name: pk_komentar; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY komentar
    ADD CONSTRAINT pk_komentar PRIMARY KEY (komentarid);


--
-- TOC entry 2008 (class 2606 OID 1253634)
-- Name: pk_pengguna; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pengguna
    ADD CONSTRAINT pk_pengguna PRIMARY KEY (userid);


--
-- TOC entry 2012 (class 2606 OID 1253636)
-- Name: pk_postingan; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY postingan
    ADD CONSTRAINT pk_postingan PRIMARY KEY (postid);


--
-- TOC entry 2016 (class 2606 OID 1253695)
-- Name: pk_suka; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY suka
    ADD CONSTRAINT pk_suka PRIMARY KEY (userid, postid);


--
-- TOC entry 2010 (class 2606 OID 1253640)
-- Name: username_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pengguna
    ADD CONSTRAINT username_uniq UNIQUE (username);


--
-- TOC entry 2021 (class 2606 OID 1253701)
-- Name: fk_post_suka_postid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY suka
    ADD CONSTRAINT fk_post_suka_postid FOREIGN KEY (postid) REFERENCES postingan(postid) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2019 (class 2606 OID 1253685)
-- Name: fk_postid_komentar_postid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY komentar
    ADD CONSTRAINT fk_postid_komentar_postid FOREIGN KEY (postid) REFERENCES postingan(postid) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2018 (class 2606 OID 1253680)
-- Name: fk_userid_komentar_userid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY komentar
    ADD CONSTRAINT fk_userid_komentar_userid FOREIGN KEY (userid) REFERENCES pengguna(userid) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2017 (class 2606 OID 1253641)
-- Name: fk_userid_pengguna_userid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY postingan
    ADD CONSTRAINT fk_userid_pengguna_userid FOREIGN KEY (userid) REFERENCES pengguna(userid) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2020 (class 2606 OID 1253696)
-- Name: fk_userid_suka_userid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY suka
    ADD CONSTRAINT fk_userid_suka_userid FOREIGN KEY (userid) REFERENCES pengguna(userid) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2149 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2019-02-14 13:55:38

--
-- PostgreSQL database dump complete
--


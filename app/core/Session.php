<?php

/**
 * 
 * 
 */
class Session
{
    public $name;
    public $data = [];
    public $userid;
    public $username;
    public $token;

    public function setSessionData($data = array()){
        $_SESSION[SITE_ID]['data'] = $data;
    }

    public function getSessionData($data = null){
        if(empty($data))
            return $_SESSION[SITE_ID]['data'];
        else
            return $_SESSION[SITE_ID]['data'][$data];
    }

    public function setSessionUserID($id){
        if(empty($id))
            $id = 'ADAFrameWork_'.$this->RandomString();

        $_SESSION[SITE_ID]['data']['userid'] = $id;
    }

    public function getSessionUserID(){
        return $_SESSION[SITE_ID]['data']['userid'];
    }

    public function setSessionUsername($username){
        if(empty($username))
            $username = 'ADAFrameWork_'.$this->RandomString();

        $_SESSION[SITE_ID]['data']['username'] = $username;
    }

    public function getSessionUsername(){
        return $_SESSION[SITE_ID]['data']['username'];
    }

    public function getSessionDataByKey($key){
        return $_SESSION[SITE_ID]['data'][$key];
    }

    public function setSessionToken($token){
        if(empty($token))
            $token = $this->RandomString(20);

        $_SESSION[SITE_ID]['aplikasi']['token'] = $token;
    }

    public function setFlash($type = 'danger', $msg = null){
        if(empty($msg))
            $msg = 'Error!';
        
        $flash = '
        <div class="alert alert-'.$type.' alert-dismissible fade show" role="alert">
            '.$msg.'
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        ';

        $_SESSION[SITE_ID]['flashdata'] = $flash;
    }

    public function getFlash(){
        $flash = $_SESSION[SITE_ID]['flashdata'];

        unset($_SESSION[SITE_ID]['flashdata']);

        return $flash;
    }

    public function RandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 1; $i <= $length; $i++) {
            $randstring = $characters[rand(0, strlen($characters))];
        }
        return $randstring;
    }
}

?>
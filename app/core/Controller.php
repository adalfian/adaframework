<?php 

/**
 * 
 */
class Controller extends Api
{
	protected $model = '';
	protected $method = '';
	protected $params = [];
	public $template_data = [];
	public $session;
	public $auth;

	function __construct(){
		
	}

	/**
	 * $location = path view
	 * $data = array data yang dikirim ke view
	 * $return = bool, returning sebagai data
	 * 
	 */
	public function view($location, $data = [], $return = false)
	{
		if($return)
			ob_start();

		require_once 'app/views/' . $location . '.php';

		if($return){
			$content = ob_get_contents();
			ob_end_clean();
			return $content;
		}
	}

	/**
	 * $model = model sesuai path
	 */
	public function model($model)
	{
		require_once 'app/models/' . $model . '.php';

		return new $model;
	}

	/**
	 * $assets = assets path
	 * $url jika berupa link
	 */
	public function assets($assets = null, $url = true)
	{
		if($url)
			return BASE_URL.'assets/'.$assets;
		else
			return 'assets/'.$assets;
	}

	/**
	 * $assets = assets path
	 * $url jika berupa link
	 */
	public function getNav($nav = null)
	{
		if(empty($nav))
			return 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

		return BASE_URL.$nav;
	}

	public function load($modul){
		$modul = ucwords($modul);
		return $this->{strtolower($modul)} = new $modul;
	}

	public function navigate($nav){
		header('Location: '.$this->getNav($nav));
		exit;
	}

	/**
	 * $url = url api
	 * $request = data api yang akan di request
	 */
	public function api_content($url, $request)
	{
		return $this->getContent($url, $request);
	}

	public function set($name, $value)
	{
		$this->template_data[$name] = $value;
	}

	public function template($template = '', $view = '' , $view_data = array(), $return = FALSE)
	{               
		$this->set('contents', $this->view($view, $view_data, TRUE));	
		$this->set('title', $view_data['title']);	
		
		return $this->view($template, $this->template_data, $return);
	}

}
 ?>
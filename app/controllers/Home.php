<?php 

/**
 * 
 */
class Home extends Controller
{
	public $session_login;
	public $pengguna;
	
	public function __construct()
	{
		$this->load('session');
		$this->load('auth');

		if(!$this->auth->isAuthenticated())
			$this->navigate('gate');

		$this->session_login = $this->session->getSessionData();
		$this->pengguna = $this->model('M_pengguna')->getData($this->session->userid);
	}

	public function index()
	{
		$data = array();
		$data['title'] = 'Dashboard';
		$data['edit'] = true;
		foreach ($this->pengguna as $key => $value) {
			$data[$key] = $value;
		}

		$kolom = array();
        $kolom[] = array('kolom' => 'isi', 'label' => '', 'type' => 'textarea', 'add' => 'rows="3" placeholder="Apa yang ingin anda tulis?"');

		$button = array();
		$button[] = array('btn' => 'primary', 'label' => 'Kirim Pos', 'type' => 'post', 'icon' => 'paper-plane', 'class' => 'float-right');

		$data['button'] = $button;
		$data['kolom'] = $kolom;
		$data['list'] = $this->model('M_postingan')->getList();

		if($_POST['act'] == 'editpos')
			$data['postedit'] = $_POST['key'];

		foreach ($data['list'] as $key => $val) {
			$suka = $this->model('M_suka')->getCountByPost($val['postid']);
			$penggunalike = $this->model('M_suka')->getData(array($this->pengguna['userid'], $val['postid']));
			$komen = $this->model('M_komentar')->getCountByPost($val['postid']);

			if(!empty($penggunalike))
				$like = false;
			else
				$like = true;

			$data['list'][$key]['banyaklike'] = (int)$suka;
			$data['list'][$key]['like'] = $like;
			$data['list'][$key]['banyakkomentar'] = (int)$komen;
		}

		if($_POST['act'] == 'posting'){
			if(empty($record))
				$record = array();
			
			foreach ($kolom as $val) {
				$record[$val['kolom']] = htmlentities($_POST[$val['kolom']]);
			}
			$record['userid'] = $session['userid'];
			$record['create_at'] = date('Y-m-d G:i:s');

			$this->model('M_postingan')->db->BeginTrans();
			
			$err = $this->model('M_postingan')->insert($record);

			$this->model('M_postingan')->db->CommitTrans((empty($err) ? true : false));

			$this->session->setFlash(($err ? 'danger' : 'success'), ($err ? 'Gagal' : 'Berhasil').' mengirim postingan');
			$this->navigate('home');
		}

		$this->template('layout/html','home/dashboard',$data);
	}

	public function teman(){
		$data = array();
		$data['title'] = 'Teman';
		$data['list'] = $this->model('M_pengguna')->getList();

		$this->template('layout/html','home/teman',$data);
	}

	public function komen($postid){
		$data = array();
		$data['title'] = 'Detail Postingan';

		$data['data'] = $this->model('M_postingan')->getData($postid);
		$suka = $this->model('M_suka')->getCountByPost($data['data']['postid']);
		$penggunalike = $this->model('M_suka')->getData(array($this->pengguna['userid'], $data['data']['postid']));
		$komen = $this->model('M_komentar')->getCountByPost($data['data']['postid']);

		if(!empty($penggunalike))
			$like = false;
		else
			$like = true;

		$data['data']['banyaklike'] = (int)$suka;
		$data['data']['like'] = $like;
		$data['data']['banyakkomentar'] = (int)$komen;

		$data['komentar'] = $this->model('M_komentar')->getByPost($postid);

		$this->template('layout/html','home/detailpost',$data);
	}

	public function action(){
		switch ($_POST['act']) {
			case 'like':
				list($postid,$like) = explode('/', $_POST['key']);
				$err = $this->like($postid,$like);
				$this->session->setFlash(($err ? 'danger' : 'success'), ($err ? 'Gagal' : 'Berhasil').' '. ($like == 1 ? 'menyukai' : 'membatakan menyukai') .' postingan');
				$this->navigate('home');
				break;
			case 'simpanedit':
				$record = array();
				$record['isi'] = htmlentities($_POST['isi']);
				$err = $this->model('M_postingan')->update($record, $_POST['key']);
				$this->session->setFlash(($err ? 'danger' : 'success'), ($err ? 'Gagal' : 'Berhasil').' menghapus postingan');
				$this->navigate('home');
				break;
			case 'hapuspos':
				$err = $this->model('M_postingan')->delete($_POST['key']);
				$this->session->setFlash(($err ? 'danger' : 'success'), ($err ? 'Gagal' : 'Berhasil').' menghapus postingan');
				$this->navigate('home');
				break;
			case 'komentar':
				$record = array();
				$record['isi'] = htmlentities($_POST['isi']);
				$record['userid'] = $this->pengguna['userid'];
				$record['postid'] = htmlentities($_POST['key']);

				$err = $this->model('M_komentar')->insert($record);
				
				$this->session->setFlash(($err ? 'danger' : 'success'), ($err ? 'Gagal' : 'Berhasil').' mengirim komentar');
				$this->navigate('home/komen/'.$_POST['key']);
				break;
		}
	}

	public function like($postid, $like = 1){
		$record = array();
		$record['userid'] = $this->pengguna['userid'];
		$record['postid'] = $postid;

		if($like == 1){
			return $this->model('M_suka')->insert($record);
		}
		else{
			return $this->model('M_suka')->delete(array($record['userid'],$record['postid']));
		}
	}
}
 ?>
<?php 

/**
 * 
 */
class Profil extends Controller
{
	
	public function __construct()
	{
		$this->load('session');
		$this->load('auth');

		if(!$this->auth->isAuthenticated())
			$this->navigate('gate');
	}

	public function index()
	{
		$session = $this->session->getSessionData();
        $pengguna = $this->model('M_pengguna')->getData($session['userid']);
        
        $r_edit = ($_POST['act'] == 'edit');
        if(isset($_POST['batal']))
            $r_edit = false;

		$data = array();
		$data['title'] = 'Profil';
		$data['edit'] = $r_edit;
		foreach ($pengguna as $key => $value) {
			$data['data'][$key] = $value;
        }
        
        $kolom = array();
        $kolom[] = array('kolom' => 'nama', 'label' => 'Nama', 'type' => 'text', 'add' => 'placeholder="Nama Lengkap" required');
        $kolom[] = array('kolom' => 'email', 'label' => 'Email', 'type' => 'email', 'add' => 'placeholder="Email: email@gmail.com" required');
        $kolom[] = array('kolom' => 'username', 'label' => 'Username', 'type' => 'text', 'add' => 'placeholder="Username" required');
        $kolom[] = array('kolom' => 'alamat', 'label' => 'Alamat', 'type' => 'text', 'add' => 'placeholder="Alamat"');
        $kolom[] = array('kolom' => 'tmplahir', 'label' => 'Tempat Lahir', 'type' => 'text', 'add' => 'placeholder="Tempat Lahir"');
        $kolom[] = array('kolom' => 'tgllahir', 'label' => 'Tanggal Lahir', 'type' => 'date', 'add' => 'placeholder="01-01-2019"');

        $data['kolom'] = $kolom;

        if($_POST['act'] == 'simpan'){
            $err = $this->simpan($_POST, $kolom);
            if(empty($err)){
                $this->session->setFlash('success', 'Simpan data pengguna berhasil.');
                $pengguna = $this->model('M_pengguna')->getData($session['userid']);
                $this->session->setSessionData($pengguna);
                $this->navigate('profil');
            }
            $this->session->setFlash('danger', 'Simpan data pengguna gagal.');
        }

		$this->template('layout/html','home/profil',$data);
    }
    
    public function simpan($post, $kolom, $record = null){
        if(empty($record))
            $record = array();
        
        foreach ($kolom as $val) {
            $record[$val['kolom']] = htmlentities($_POST[$val['kolom']]);
        }

        $key = $this->session->getSessionUserID();

        $this->model('M_pengguna')->db->BeginTrans();
        
        $err = $this->model('M_pengguna')->update($record, $key);

        $this->model('M_pengguna')->db->CommitTrans((empty($err) ? true : false));

        return $err;
    }
}
 ?>
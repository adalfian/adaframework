<?php 

/**
 * 
 */
class Gate extends Controller
{
	
	function __construct()
	{
        $this->load('session');
		$this->load('auth');
	}

	public function index(){
        if($this->auth->isAuthenticated())
            $this->navigate('home/');
            
        $data = array();
        $data['title'] = 'Login';

        $this->view('login', $data);
    }

    public function daftar($tes){
        if($this->auth->isAuthenticated())
            $this->navigate('home/');
            
        $data = array();
        $data['title'] = 'Daftar';

        $this->view('daftar', $data);
    }

    public function goLogin(){
        if($_POST['act'] == 'login'){
            $username = $_POST['username'];
            $password = $_POST['password'];

            $data = $this->model('M_pengguna')->getUserByUsername($username);
            if(!empty($data)){
                if($data['password'] == $password){
                    $this->session->setSessionData($data);
                    $this->session->setFlash('success', 'Login berhasil!');
                    $this->navigate('home/');
                } 
                else {
                    $this->session->setFlash('danger', 'Login gagal. Password tidak sesuai.');
                    $this->navigate('gate/');
                }
            }
            
            $this->session->setFlash('danger', 'Login gagal. Username tidak ditemukan');
            $this->navigate('gate/');
        }
    }

    public function goDaftar(){
        if($_POST['act'] == 'daftar'){
            $data = array();
            $data['nama'] = htmlentities($_POST['nama']);
            $data['email'] = htmlentities($_POST['email']);
            $data['username'] = htmlentities($_POST['username']);
            $data['password'] = htmlentities($_POST['password']);
            $data['jk'] = htmlentities($_POST['jk']);

            $err = $this->model('M_pengguna')->insert($data);

            if($err){
                $error = 'danger';
                $msg = 'Gagal Mendaftar';
            }
            else{
                $error = 'success';
                $msg = 'Berhasil Mendaftar!<br> Silahkan login menggunakan username dan password';
            }
            
            $this->session->setFlash($error, $msg);
            $this->navigate('gate/'.($err?'daftar':''));
        }
    }

    public function logout(){
        unset($_SESSION[SITE_ID]['data']);

        $this->session->setFlash('success', 'Berhasil Logout!');
        $this->navigate('gate/');
    }

}
<?php 

/**
 * 
 */
class M_komentar extends Model
{
	protected static $table = 'komentar';
	protected static $schema = '';
	protected static $key = 'komentarid';
	protected static $order = 'komentarid desc';

	public function getCountByPost($postid){
		$sql = "select count(1) from " . $this->getTable() . " where postid = " . $this->escape($postid);

		return $this->getOne($sql);
	}

	public function getByPost($postid){
		$sql = "select k.*, p.nama, to_char(k.create_at, 'HH24:MM:SS DD-MM-YYYY') as timepost from " . $this->getTable() . " k 
				join " . $this->getTable('pengguna') . " p using(userid)
				where k.postid = " . $this->escape($postid);

		return $this->getArray($sql);
	}
}
 ?>
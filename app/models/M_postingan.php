<?php 

/**
 * 
 */
class M_postingan extends Model
{
	protected static $table = 'postingan';
	protected static $schema = '';
	protected static $key = 'postid';
	protected static $order = 'postid desc';

    public function getListSql() {
        $sql = "select ps.*, to_char(ps.create_at, 'HH24:MM:SS DD-MM-YYYY') as timepost, p.nama from " . $this->getTable() . " ps
                join pengguna p using(userid)";

		return $sql;
    }

    public function getDataSql() {
        $sql = "select ps.*, to_char(ps.create_at, 'HH24:MM:SS DD-MM-YYYY') as timepost, p.nama from " . $this->getTable() . " ps
                join pengguna p using(userid)";

		return $sql;
    }
    
	public function getPostByUser($userid)
	{
        $sql = "select * from " . $this->getTable() . " where userid = " . $this->escape($userid) . "";
		$row = $this->getArray($sql);

		return $row;
	}
}
 ?>
<div class="card">
    <div class="card-body">
      <h4>Daftar Teman</h4>
      <hr>
      <div class="row">
        <?php 
        foreach ($data['list'] as $key => $value) {
          if($value['userid'] == $this->session->getSessionUserID())
            continue;
        ?>
        <div class="col-md-6">
          <div class="card mb-3">
            <div class="row no-gutters">
              <div class="col-md-3">
                <img src="<?= $this->assets('img/default.png', true) ?>" class="card-img" alt="...">
              </div>
              <div class="col-md-9">
                <div class="card-body">
                  <h5 class="card-title"><?= $value['nama'] ?></h5>
                </div>
              </div>
              <div class="col-md-12">
                <div class="card-body">
                  <p class="card-text" >
                    <!-- <ul class="list-group list-group-flush">
                      <li class="list-group-item" style="font-size:13px;">
 -->                      <span class="text-muted">Alamat</span><br>
                          <strong><?= $value['alamat'] ? $value['alamat'] : '&nbsp;' ?></strong><hr>
                      <!-- </li> -->
                      <!-- <li class="list-group-item"> -->
                          <span class="text-muted">Email</span><br>
                          <strong><?= $value['email'] ? $value['email'] : '&nbsp;'  ?></strong><hr>
                      <!-- </li> -->
                      <!-- <li class="list-group-item"> -->
                          <span class="text-muted">Jenis Kelamin</span><br>
                          <strong><?= $value['jk'] == 'L' ? 'Laki-laki' : 'Perempuan' ?></strong><hr>
                      <!-- </li> -->
                    <!-- </ul> -->
                  </p>
                  <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>
</div>

<?php $postingan = $data['data'];?>
<div class="card">
    <div class="card-body">
        <div class="card">
        	<div class="card-header">
	        	<span class="card-title"><strong><?= $postingan['nama'] ?></strong><br><?= $postingan['timepost'] ?></span>
        	</div>
        	<div class="card-body">
        		<p>
	                <?= htmlentities($postingan['isi']) ?>
	            </p>
        	</div>
            <div class="card-footer">
            	<button class="btn btn-sm btn-<?= $value['like'] ? 'primary' : 'info' ?>" data-type="suka" data-id="<?= $value['postid'].'/'.($value['like'] ? '1' : '0') ?>">
                    <?= !empty($postingan['banyaklike']) ? $postingan['banyaklike'] : '' ?> <i class="far fa-thumbs-up"></i> 
                    <?= $postingan['like'] ? 'Suka' : 'Batal Suka' ?>
                </button>
                <span class="btn btn-sm btn-info">
                    <?= !empty($postingan['banyakkomentar']) ? $postingan['banyakkomentar'] : '' ?> <i class="far fa-comment-dots"></i> 
                    Komentar
                </span>
            </div>
        </div>
        <div class="card">
        	<div class="card-body">
        		<form action="<?= $this->getNav('home/action') ?>" method="post" id="form_komentar">
		        	Tulis komentar
		            <textarea name="isi"  rows="3" class="form-control" placeholder="Tulis komentar disini..."></textarea>
		            <span class="float-right">
		            	<span class="btn btn-sm btn-primary" data-type="kirimkomen" data-id="<?= $postingan['postid'] ?>">
	                        <i class="fas fa-paper-plane"></i> Kirim komentar
	                    </span>
		            </span>
		            <input type="hidden" name="act" id="act">
        			<input type="hidden" name="key" id="key">
	            </form>
        	</div>
        	<div class="card-body">
        		<?php foreach ($data['komentar'] as $val) {?>
        		<div class="card">
        			<div class="card-body">
			        	<span class="card-title"><strong><?= $val['nama'] ?></strong><br><?= $val['timepost'] ?></span>
			        	<hr>
			        	<p>
			                <?= htmlentities($val['isi']) ?>
			            </p>
        			</div>
        		</div>
        		<?php }?>
        	</div>
        </div>
    </div>
</div>

<script type="text/javascript">
	var komentar = $('#form_komentar');

    $('[data-type="kirimkomen"]').click(function(){
    	komentar.find('#key').val($(this).attr('data-id'));
        goSubmit(komentar, 'komentar');
    });

</script>
<div class="card">
    <div class="card-body">
        <div class="card">
            <?php $this->view('layout/inc_data', $data); ?>
        </div>
        <hr>
        <h5>Postingan</h5>
        <form method="post" action="<?= $this->getNav('home/action') ?>" id="form_post">
        <?php 
        foreach ($data['list'] as $key => $value) {
        ?>
        <div class="card" style="margin-bottom:10px">
            <div class="card-body">
                <span class="card-title"><strong><?= $value['nama'] ?></strong><br><?= $value['timepost'] ?></span>
                
                <hr>
                <p>
                    <?php if($data['postedit'] == $value['postid']){ ?>
                        <textarea name="isi" rows="3" class="form-control"><?= htmlentities($value['isi']) ?></textarea>
                    <?php } else {?>
                        <?= htmlentities($value['isi']) ?>
                    <?php }?>
                </p>
            </div>
            <div class="card-footer">
                <div class="row">
                    <?php if($data['postedit'] == $value['postid']){ ?>
                        <div class="col-md-6 col-sm-6">
                            &nbsp;
                        </div>
                        <div class="col-md-6 col-sm-6 float-right">
                            <span class="text-muted float-right">
                                <button type="submit" data-type="simpanedit" class="btn btn-sm btn-success"><i class="far fa-save"></i> Simpan</button>
                                <span data-type="bataledit" class="btn btn-sm btn-warning"><i class="fas fa-undo"></i> Batal</span>
                            </span>
                        </div>
                    <?php } else {?>
                        <div class="col-md-6 col-sm-6">
                            <button class="btn btn-sm btn-<?= $value['like'] ? 'primary' : 'info' ?>" data-type="suka" data-id="<?= $value['postid'].'/'.($value['like'] ? '1' : '0') ?>">
                                <?= !empty($value['banyaklike']) ? $value['banyaklike'] : '' ?> <i class="far fa-thumbs-up"></i> 
                                <?= $value['like'] ? 'Suka' : 'Batal Suka' ?>
                            </button>
                            <span class="btn btn-sm btn-info" data-type="komen" data-id="<?= $value['postid'] ?>">
                                <?= !empty($value['banyakkomentar']) ? $value['banyakkomentar'] : '' ?> <i class="far fa-comment-dots"></i> 
                                Komentar
                            </span>
                        </div>
                        <?php if($value['userid'] == $this->session->getSessionUserID()){ ?>
                        <div class="col-md-6 col-sm-6">
                            <span class="text-muted float-right">
                                <button class="btn btn-sm btn-warning" data-type="editpos" data-id="<?= $value['postid'] ?>" data-toggle="tooltip" data-title="Ubah"><i class="fas fa-edit"></i></button>
                                <span class="btn btn-sm btn-danger" data-type="hapuspos" data-id="<?= $value['postid'] ?>" data-toggle="tooltip" data-title="Hapus"><i class="fas fa-trash"></i></span>
                            </span>
                        </div>
                        <?php } ?>
                    <?php }?>
                </div>
                
            </div>
        </div>
        <?php }?>
        <input type="hidden" name="act" id="act">
        <input type="hidden" name="key" id="key">
        </form>
        <br>
<!--         <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
                <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">Previous</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                <a class="page-link" href="#">Next</a>
                </li>
            </ul>
        </nav> -->
    </div>
</div>
<script>
    var posting = $('#form_post');

    $('[data-type="post"]').click(function(){
        goSubmit($('#form_data'), 'posting');
    });
    $('[data-type="suka"]').click(function(){
        posting.find('#key').val($(this).attr('data-id'));
        goSubmit(posting, 'like');
    });
    $('[data-type="komen"]').click(function(){
        location.href = '<?= $this->getNav('home/komen/') ?>' + $(this).attr('data-id');
    });
    $('[data-type="editpos"]').click(function(){
        posting.attr('action', '<?= $this->getNav('home') ?>');
        posting.find('#key').val($(this).attr('data-id'));
        goSubmit(posting, 'editpos');
    });
    $('[data-type="hapuspos"]').click(function(){
        var ok = confirm('Apakah anda yakin akan menghapus postingan ini');
        if(ok){
            posting.find('#key').val($(this).attr('data-id'));
            goSubmit(posting, 'hapuspos');
        }
    });
    $('[data-type="bataledit"]').click(function(){
        location.href ='<?= $this->getNav() ?>';
    });
    $('[data-type="simpanedit"]').click(function(){
        posting.find('#key').val('<?= $data['postedit'] ?>');
        goSubmit(posting, 'simpanedit');
    });
</script>
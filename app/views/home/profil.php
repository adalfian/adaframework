
<div class="card">
    <div class="card-header">
        Profil
    </div>
    <div class="card-body">
        <?php $this->view('layout/inc_data', $data); ?>
    </div>
</div>
<script>
$("[name='username']").alphanum({
    allowSpace: false,
    allowUpper: false
});
</script>

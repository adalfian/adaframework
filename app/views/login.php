<html>
    <head>
        <title><?= $data['title'] ?></title>
        <link rel="stylesheet" href="<?= $this->assets('css/bootstrap.min.css', true) ?>">
        <link rel="stylesheet" href="<?= $this->assets('plugins/fontawesome/css/all.css', true) ?>">
        <style>
        .login{
            margin-top: 30px;
        }
        </style>
    </head>
    <body>
    <div class="container">
        <div class="col-md-12 text-center">
            <h1>Sevima Academy</h1>
        </div>
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
                <div class="card mb-3 login">
                    <h5 class="card-header"><?= $data['title'] ?></h5>
                    <div class="card-body">
                        <form method="post" action="<?= $this->getNav('gate/goLogin'); ?>">
                            <?php 
                            $flash = $this->session->getFlash();
                            if(!empty($flash))
                                echo $flash;
                            ?>
                            <div class="form-group">
                                <label for="username" class="col-md-12 col-form-label">Username</label>
                                <div class="col-md-12">
                                <input type="username" name="username" class="form-control" id="username" placeholder="Username">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="password" class="col-md-12 col-form-label">Password</label>
                                <div class="col-md-12">
                                <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                                </div>
                            </div>
                            <div class="form-group  text-center">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-success btn-block"><i class="fas fa-sign-in-alt"></i> Masuk</button>
                                </div>
                            </div>
                            <div class="form-group  text-center">
                                <div class="col-md-12">
                                    Belum punya akun?<br>
                                    <a href="<?= $this->getNav('gate/daftar') ?>" class="btn btn-info"><i class="fas fa-user-plus"></i> Daftar Sekarang</a>
                                </div>
                            </div>
                            <input type="hidden" name="act" id="act" value="login">
                        </form>
                        <div class="card-footer text-muted">
                            <div class="footer-copyright text-center py-3">
                                © 2019 Copyright:
                                <a href="#"> ADA Framework</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div>
    <script src="<?= $this->assets('js/jquery-3.3.1.min.js', true) ?>"></script>
    <script src="<?= $this->assets('js/jquery.alphanum.js', true) ?>"></script>
    <script src="<?= $this->assets('js/popper.min.js', true) ?>"></script>
    <script src="<?= $this->assets('js/bootstrap.min.js', true) ?>"></script>
    <script>
    $('.alert').alert();
    $("[name='username']").alphanum({
        allowSpace: false,
        allowUpper: false
    });
    </script>
    </body>
</html>

<form action="" method="post" id="form_data">
    <style>
    .form-control-plaintext{
        border-bottom:1px solid;
    }
    </style>
    <?php foreach ($data['kolom'] as $col) {?>
    <div class="form-group">
        <label for="<?= $col['kolom'] ?>" class="col-md-12 col-form-label"><?= $col['label'] ?></label>
        <div class="col-md-12">
            <?php if($col['type'] == 'textarea'){ ?>
                <textarea type="<?= $col['type'] ?>" name="<?= $col['kolom'] ?>" class="form-control<?= $data['edit'] ? '' : '-plaintext' ?>" id="<?= $col['kolom'] ?>" value="<?= htmlentities($data['data'][$col['kolom']]) ?>" <?= !empty($col['add']) ? $col['add'] : '' ?> <?= $data['edit'] ? '' : 'readonly' ?> ></textarea>
            <?php } else {?>
            <input type="<?= $col['type'] ?>" name="<?= $col['kolom'] ?>" class="form-control<?= $data['edit'] ? '' : '-plaintext' ?>" id="<?= $col['kolom'] ?>" value="<?= htmlentities($data['data'][$col['kolom']]) ?>" <?= !empty($col['add']) ? $col['add'] : '' ?> <?= $data['edit'] ? '' : 'readonly' ?>>
            <?php }?>
        </div>
    </div>
    <?php } ?>
    <hr>
    <div class="form-group  text-center">
        <?php if($data['edit'] and empty($data['button'])){ ?>
            <div class="col-md-12">
                <button class="btn btn-success" data-type="simpan" data-id="simpan"><i class="far fa-save"></i> Simpan</button>
                <button class="btn btn-warning" data-type="batal" data-id="batal"><i class="fas fa-undo"></i> Batal</button>
            </div>
        <?php } elseif(!$data['edit'] and empty($data['button'])) { ?>
            <div class="col-md-12">
                <button class="btn btn-warning" data-type="edit" data-id="edit"><i class="fas fa-edit"></i> Ubah</button>
            </div>
        <?php }?>
        <?php if(!empty($data['button'])){?>
            <div class="col-md-12">
                <?php foreach ($data['button'] as $val) {?>
                    <button class="btn btn-<?= $val['btn'] ?> <?= $val['class'] ?>" data-type="<?= $val['type'] ?>" data-id="<?= $val['type'] ?>" <?= $val['add'] ?>><i class="fas fa-<?= $val['icon'] ?>"></i> <?= $val['label'] ?></button>
                <?php }?>
            </div>
        <?php }?>

        <input type="hidden" name="act" id="act">
    </div>
</form>
<script>
$('[data-type="simpan"]').click(function(){
    var act = $(this).attr('data-id');
    if(act == '')
        act = 'simpan';
    
    goSubmit($('#form_data'), act);
});
$('[data-type="batal"]').click(function(){
    var act = $(this).attr('data-id');
    if(act == '')
        act = 'batal';

    goSubmit($('#form_data'), act);
});
$('[data-type="edit"]').click(function(){
    var act = $(this).attr('data-id');
    if(act == '')
        act = 'edit';

    goSubmit($('#form_data'), act);
});
</script>
<!-- Footer -->
<footer class="page-footer font-small" style="background-color: #f8f9fa">

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">
    © 2019 Copyright <?= APP_NAME ?>
    <br>
    Powered by <a href="#"> ADA Framework</a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->
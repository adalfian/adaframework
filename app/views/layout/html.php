<html>
    <head>
        <title><?= $data['title'] ?> | <?= APP_NAME ?></title>
        <link rel="stylesheet" href="<?= $this->assets('css/bootstrap.min.css', true) ?>">
        <link rel="stylesheet" href="<?= $this->assets('plugins/fontawesome/css/all.css', true) ?>">
        <link rel="icon" type="img/png" href="<?= $this->assets('img/logo.png') ?>" sizes="16x16" />
        <script src="<?= $this->assets('js/jquery-3.3.1.min.js', true) ?>"></script>
        <script src="<?= $this->assets('js/jquery.alphanum.js', true) ?>"></script>
        <script src="<?= $this->assets('js/popper.min.js', true) ?>"></script>
        <script src="<?= $this->assets('js/bootstrap.min.js', true) ?>"></script>
    </head>
    <body>
        <?php include('navigation.php'); ?>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <?php include('sidebar.php'); ?>
                </div>
                <div class="col-md-9">
                    <?php 
                    $flash = $this->session->getFlash();
                    if(!empty($flash))
                        echo $flash;
                    ?>
                    <?= $data['contents'] ?>
                </div>
            </div>
        </div>
        <?php include('footer.php'); ?>
        
        <script>
        $(function () {
          $('[data-toggle="tooltip"]').tooltip();
          $('.alert').alert()
        });
        function goSubmit(elem, action){
            $(elem).find('#act').val(action);
            $(elem).submit();
        }
        </script>
    </body>
</html>
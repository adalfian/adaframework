<?php
$session = $this->session->getSessionData();
?>
<div class="card">
    <img src="<?= $this->assets('img/default.png', true) ?>" class="card-img-top" alt="...">
    <div class="card-body">
        <h5 class="card-title">Hai, <br><?= $session['nama'] ?></h5>
    </div>
    <ul class="list-group list-group-flush">
        <li class="list-group-item">
            <span class="text-muted">Username</span><br>
            <strong><?= $session['username'] ?></strong>
        </li>
        <li class="list-group-item">
            <span class="text-muted">Email</span><br>
            <strong><?= $session['email'] ?></strong>
        </li>
    </ul>
    <div class="card-body text-center">
        <a href="<?= $this->getNav('profil') ?>" class="card-link btn btn-sm btn-info"><i class="far fa-edit"></i> Profil</a>
        <a href="<?= $this->getNav('gate/logout') ?>" class="card-link btn btn-sm btn-danger"><i class="fas fa-power-off"></i> Logout</a>
    </div>
</div>
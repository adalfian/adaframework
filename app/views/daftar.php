<html>
    <head>
        <title><?= $data['title'] ?></title>
        <link rel="stylesheet" href="<?= $this->assets('css/bootstrap.min.css', true) ?>">
        <link rel="stylesheet" href="<?= $this->assets('plugins/fontawesome/css/all.css', true) ?>">
        <style>
        .login{
            margin-top: 30px;
        }
        </style>
    </head>
    <body>
    <div class="container">
        <div class="col-md-12 text-center">
            <h1>Sevima Academy</h1>
        </div>
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
                <div class="card mb-3 login">
                    <h5 class="card-header"><?= $data['title'] ?></h5>
                    <div class="card-body">
                        <form method="post" action="<?= $this->getNav('gate/goDaftar'); ?>">
                            <?php 
                            $flash = $this->session->getFlash();
                            if(!empty($flash))
                                echo $flash;
                            ?>
                            <div class="form-group">
                                <label for="username" class="col-md-12 col-form-label">Nama</label>
                                <div class="col-md-12">
                                <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama Lengkap" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-md-12 col-form-label">Email</label>
                                <div class="col-md-12">
                                <input type="email" name="email" class="form-control" id="email" placeholder="Email"required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="username" class="col-md-12 col-form-label">Username</label>
                                <div class="col-md-12">
                                <input type="text" name="username" class="form-control" id="username" placeholder="Username"required>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="password" class="col-md-12 col-form-label">Password</label>
                                <div class="col-md-12">
                                <input type="password" name="password" class="form-control" id="password" placeholder="Password"required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="jk" class="col-md-12 col-form-label">Jenis Kelamin</label>
                                <div class="col-md-12">
                                <select class="form-control" id="jk" name="jk">
                                    <option value="L">Laki-laki</option>
                                    <option value="P">Perempuan</option>
                                </select>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group  text-center">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-success btn-block"><i class="fas fa-user-plus"></i> Daftar Sekarang</button>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group  text-center">
                                <div class="col-md-12">
                                    Sudah punya akun? 
                                    <a href="<?= $this->getNav('gate/') ?>" class="btn btn-info"><i class="fas fa-sign-in-alt"></i> Masuk</a>
                                </div>
                            </div>
                            <input type="hidden" name="act" id="act" value="daftar">
                        </form>
                        <div class="card-footer text-muted">
                            <div class="footer-copyright text-center py-3">
                                © 2019 Copyright:
                                <a href="#"> ADA Framework</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div>
    <script src="<?= $this->assets('js/jquery-3.3.1.min.js', true) ?>"></script>
    <script src="<?= $this->assets('js/jquery.alphanum.js', true) ?>"></script>
    <script src="<?= $this->assets('js/popper.min.js', true) ?>"></script>
    <script src="<?= $this->assets('js/bootstrap.min.js', true) ?>"></script>
    <script>
    $('.alert').alert();
    $("[name='username']").alphanum({
        allowSpace: false,
        allowUpper: false
    });
    </script>
    </body>
</html>
